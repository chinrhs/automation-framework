package automation.glue;

import automation.config.AutomationFrameworkConfiguration;
import automation.driver.DriverSingleton;
import automation.pages.CheckOutPage;
import automation.pages.HomePage;
import automation.pages.SignInPage;
import automation.utils.ConfigurationProperties;
import automation.utils.Constants;
import automation.utils.TestCases;
import automation.utils.Utils;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@ContextConfiguration(classes = AutomationFrameworkConfiguration.class)
public class StepDefinition {
    private WebDriver driver;
    private CheckOutPage checkOutPage;
    private HomePage homePage;
    private SignInPage signInPage;
    private ExtentTest test;
    static ExtentReports report = new ExtentReports("report/TestReport.html");


    @Autowired
    ConfigurationProperties configurationProperties ;

    @Before
    public void initializeObjects(){
        DriverSingleton.getInstance("Chrome");
        homePage = new HomePage();
        signInPage = new SignInPage();
        checkOutPage = new CheckOutPage();

        TestCases[] tests = TestCases.values();
        test = report.startTest(tests[Utils.count].getTestsName());
        Utils.count++;
    }

    @Given("^I go to the Website")
    public void I_go_to_the_Website(){
        driver = DriverSingleton.getDriver();
        driver.get(Constants.URL);
        test.log(LogStatus.PASS, "Navigatin to " + Constants.URL);
    }

    @When("^I click on Sign In Button")
    public void I_click_on_Sign_In_Button(){
        homePage.clickOnSignIn();
        test.log(LogStatus.PASS, "SignIn button has been clicked");

    }

    @When("^I add two elements to the cart")
    public void I_Add_Two_Elements_To_The_Cart(){
        homePage.addFirstElementToCart();
        homePage.addSecondElementTOCart();
        test.log(LogStatus.PASS, "Two elements added to the cart");

    }

    @And("^I specify my credentials and click LogIn")
    public void I_specify_my_credentials_and_click_LogIn(){
        signInPage.login("laurentiu.raducu@gmail.com", "MTIzNDU2");
        test.log(LogStatus.PASS, "Login has been clicked");

    }

    @And("^I proceed to checkout")
    public void I_Proceed_To_Checkout(){
        checkOutPage.goToAuthenticationPage();
        test.log(LogStatus.PASS, "We proceed to checkout");

    }

    @And("^I confirm address, shipping, payment and final order")
    public void I_Confirm_Address_Shipping_Payment_And_Final_Order(){
        checkOutPage.goToShippingPage();
        checkOutPage.goToChoosePaymentMethodPage();
        checkOutPage.goToOrderSummaryPage();
        checkOutPage.goToOrderConfirmationPage();
        test.log(LogStatus.PASS, "We confirm the final order");

    }



    @Then("^I can log into the Website")
    public void I_can_log_into_the_Website(){
        if(homePage.getUserName().equals("Laurentiu Raducu")){
            test.log(LogStatus.PASS, "This authentication is successful");
        }else{
        test.log(LogStatus.FAIL, "Authentication is not successfull");}
        assertEquals("Laurentiu Raducu", homePage.getUserName());
    }

    @Then("^the elements are bought")
    public void the_Elements_Are_Bought(){
        if(checkOutPage.checkFinalStatus()){
            test.log(LogStatus.PASS, "Two items were successfully bought");
        }else {
        test.log(LogStatus.FAIL, "Items were not bought");}
        assertTrue(checkOutPage.checkFinalStatus());}



@After
    public void  closeObjects(){

        report.endTest(test);
        report.flush();
        DriverSingleton.closeObjectInstance();
}













}



