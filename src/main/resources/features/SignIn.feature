Feature: Shopping Automation
  Scenario: Testing the authentication
    Given I go to the Website
    When I click on Sign In Button
    And I specify my credentials and click LogIn
    Then I can log into the Website



    Scenario: Testing the purchase of two items
      Given I go to the Website
      When I add two elements to the cart
      And I proceed to checkout
      And I specify my credentials and click LogIn
      And I confirm address, shipping, payment and final order
      Then the elements are bought