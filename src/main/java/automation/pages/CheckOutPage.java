package automation.pages;

import automation.driver.DriverSingleton;
import automation.utils.Constants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CheckOutPage {
    private WebDriver driver;

    public CheckOutPage(){
        driver = DriverSingleton.getDriver();
        PageFactory.initElements(driver,this);
    }

    @FindBy(xpath = "//*[@id=\"cart_title\"]/text()")
    private WebElement summaryPageTitle;

    @FindBy(css ="#center_column > p.cart_navigation.clearfix > a.button.btn.btn-default.standard-checkout.button-medium")
    private WebElement proceedToCheckoutButtonCartSummary;

    @FindBy(css = "#center_column > form > p > button")
    private WebElement proceedToCheckoutButtonAddressPage;

    @FindBy(id="uniform-cgv")
    private WebElement confirmShippingCheckbox;

    @FindBy(css = "#form > p > button")
    private WebElement proceedToCheckoutButtonShippingPage;

    @FindBy(css = "#HOOK_PAYMENT > div:nth-child(1) > div")
    private WebElement payByBankWireOption;

    @FindBy(css = "#cart_navigation > button")
    private WebElement confirmOrderButton;

    @FindBy(css ="#center_column > div > p > strong")
    private WebElement orderConfirmationMessage;

    @FindBy(id="summary_products_quantity")
    private  WebElement cartSummaryStatement;

    public String GetOrderConfirmationMessage(){
        return orderConfirmationMessage.getText();
    }

    public String getCartSummaryStatement(){
        return cartSummaryStatement.getText();
    }

    public Boolean checkPageTitle(String title){
       return summaryPageTitle.getText().equals(title);
    }

    public void goToAuthenticationPage(){
        WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT );
        wait.until(ExpectedConditions.elementToBeClickable(proceedToCheckoutButtonCartSummary));
        proceedToCheckoutButtonCartSummary.click();
    }

    public void goToShippingPage(){
        WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT );
        wait.until(ExpectedConditions.elementToBeClickable(proceedToCheckoutButtonAddressPage));
        proceedToCheckoutButtonAddressPage.click();
    }

    public void goToChoosePaymentMethodPage(){
        WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT );
        wait.until(ExpectedConditions.elementToBeClickable(confirmShippingCheckbox));
        confirmShippingCheckbox.click();
        wait.until(ExpectedConditions.elementToBeClickable(proceedToCheckoutButtonShippingPage));
        proceedToCheckoutButtonShippingPage.click();
    }

    public void goToOrderSummaryPage(){
        WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT );
        wait.until(ExpectedConditions.elementToBeClickable(payByBankWireOption));
        payByBankWireOption.click();
    }

    public void goToOrderConfirmationPage(){
        WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT );
        wait.until(ExpectedConditions.elementToBeClickable(confirmOrderButton));
        confirmOrderButton.click();
    }

    public Boolean checkFinalStatus(){
        WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT );
        wait.until(ExpectedConditions.elementToBeClickable(orderConfirmationMessage));
        return orderConfirmationMessage.getText().contains(Constants.Order_Confirmation_Message);

    }













}
