package automation.pages;

import automation.driver.DriverSingleton;
import automation.utils.Constants;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {
    private final WebDriver driver;

    public HomePage() {
        this.driver = DriverSingleton.getDriver();
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "#homefeatured > li.ajax_block_product.col-xs-12.col-sm-4.col-md-3.first-in-line.first-item-of-tablet-line.first-item-of-mobile-line > div > div.left-block > div > a.product_img_link > img")
    private WebElement firstItemElement;
    @FindBy(css = "#homefeatured > li:nth-child(2) > div > div.left-block > div > a.product_img_link > img")
    private WebElement secondItemElement;
    @FindBy(css = "#add_to_cart > button")
    private WebElement addToCartButton;
    @FindBy(css = "#layer_cart > div.clearfix > div.layer_cart_cart.col-xs-12.col-md-6 > div.button-container > span")
    private WebElement continueShoppingButton;
    @FindBy(css = "#layer_cart > div.clearfix > div.layer_cart_cart.col-xs-12.col-md-6 > div.button-container > a")
    private WebElement proceedToCheckoutButton;
    @FindBy(css = "#header > div:nth-child(3) > div > div > div:nth-child(3) > div > a")
    private WebElement cart;
    @FindBy(css = "#header_logo > a > img")
    private WebElement yourLogoElement;
    @FindBy(css = "#header > div.nav > div > div > nav > div:nth-child(1) > a > span")
    private WebElement userName;
    @FindBy(css="#header > div.nav > div > div > nav > div.header_user_info")
    private WebElement signInButton;

    @FindBy(id="search_query_top")
    private WebElement searchBox;
    @FindBy(css="#searchbox > button")
    private WebElement searchButton;
    @FindBy(css="#center_column > ul > li > div > div.left-block > div > a.product_img_link > img")
    private WebElement searchResultElement;


    public Boolean findSearchElement(String input){
        WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);
        wait.until(ExpectedConditions.elementToBeClickable(searchBox));
        searchBox.sendKeys(input);
        searchButton.click();
        try {
            if(searchResultElement.isEnabled()){
                return true;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }


    public void clickOnSignIn() {
        WebDriverWait wait = new WebDriverWait(driver,Constants.TIMEOUT);
        wait.until(ExpectedConditions.elementToBeClickable(signInButton));
        signInButton.click();
    }

    public String getUserName() {
       return  userName.getText();
    }


    public void addFirstElementToCart() {
        WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);
        firstItemElement.click();
        wait.until(ExpectedConditions.elementToBeClickable(addToCartButton));
        addToCartButton.click();
        wait.until(ExpectedConditions.elementToBeClickable(continueShoppingButton));
        continueShoppingButton.click();
        if (cart.getText().contains("1 Product")) {
            System.out.println("Cart has been updated");
        } else {
            System.out.println("Cart has not been updated");
        }
    }

    public void addSecondElementTOCart() {
        yourLogoElement.click();
        WebDriverWait wait = new WebDriverWait(driver, Constants.TIMEOUT);
        wait.until(ExpectedConditions.elementToBeClickable(secondItemElement));
        secondItemElement.click();
        wait.until(ExpectedConditions.elementToBeClickable(addToCartButton));
        addToCartButton.click();
        wait.until(ExpectedConditions.elementToBeClickable(proceedToCheckoutButton));
        proceedToCheckoutButton.click();

    }


}
