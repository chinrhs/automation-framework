package automation.pages;

import automation.driver.DriverSingleton;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import automation.utils.Utils;

public class SignInPage {
    private WebDriver driver;


    public SignInPage(){
        this.driver = DriverSingleton.getDriver();
        PageFactory.initElements(driver,this);
    }


    @FindBy(id="email")
    private WebElement signInEmailField;

    @FindBy(id="passwd")
    private WebElement signInPasswordField;

    @FindBy(id="SubmitLogin")
    private WebElement signInButton;


    @FindBy(id="email_create")
    private WebElement signUpEmailField;

    @FindBy(id="SubmitCreate")
    private WebElement createAnAccountButton;


    public void login(String email, String password){
        signInEmailField.sendKeys(email);
        signInPasswordField.sendKeys(Utils.decoder(password));
        signInButton.click();
    }

    public void signUp(String email){
        signUpEmailField.sendKeys(email);
        signUpEmailField.click();
    }















}
