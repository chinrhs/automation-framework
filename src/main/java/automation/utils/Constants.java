package automation.utils;

public class Constants {
    public static final String Prop_File_Name = "framework.properties";
    public static final String File_Not_Found_Message = "The property file has not been found";
    public static final String CHROME = "Chrome";
    public static final String FIREFOX = "Firefox";
    public static final String PHANTOMJS = "PhantonJs";
    public static final int  TIMEOUT = 15;
    public static final String Order_Confirmation_Message = "Your order on My Store is complete.";
    public static final String Title_Of_Summary_Page = "SHOPPING-CART SUMMARY";
    public static final String EMAIL ="email";
    public static final String PASSWORD ="password";

    public static final String URL = "http://automationpractice.com";
    public static final String USERNAME = "username";
    public static final String BROWSER = "browser";


    public static final String Cart_Summary_String = "2 Products";
}
