package automation.utils;

public enum TestCases {
    T1("Test user authentication"),
    T2("Test to buy two items.");


    private String testsValue;

    TestCases(String name) {
        this.testsValue = name;
    }

    public String getTestsName() {
        return testsValue;
    }
}
