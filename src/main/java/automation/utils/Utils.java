package automation.utils;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class Utils {
    public static int count = 0;

    public static String decoder(String encodedString){
        Base64.Decoder decoder = Base64.getDecoder();
        return new String(decoder.decode(encodedString.getBytes(StandardCharsets.UTF_8)));

    }
}
