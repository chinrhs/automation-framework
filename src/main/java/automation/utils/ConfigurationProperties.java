package automation.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;


@Component
@PropertySource("framework.properties")
public class ConfigurationProperties {

    @Value("${browser}")
    public String browser;

    @Value("${email}")
    public String email;

    @Value("${password}")
    public String password;

    @Value("${username}")
    public String username;

    public String getBrowser() {return browser;}

    public String getEmail() {return email;}

    public String getPassword() {return password;}

    public String getUsername() {return username;}
}
